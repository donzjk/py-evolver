import wx
import time
from evolver import Evolver
import pstats
import threading
import primitives
import sys

PROFILING = False


class ImageUpdater(threading.Thread):
    """Handles updating of the images in the GUI. We don't want to update the
    scene with every tick, but rather request images from evolver regularly.
    We schedule two recurring functions that update the two images regularly.
    """

    def __init__(self):
        threading.Thread.__init__(self)
        self.watches = []
        self.paused = True
        # thread control
        self.updater_running = False

    def schedule(self, func, interval):
        """Schedule a repeating function call."""
        self.watches.append([time.time() + interval, func, interval])

    def remove(self, func):
        """Remove a function call."""
        for x in self.watches[:]:
            if x[1] == func:
                self.watches.remove(x)
                return

    def run(self):
        """Run updater thread."""
        self.updater_running = True
        while self.updater_running:
            if not self.paused:
                ct = time.time()
                for x in self.watches:
                    if ct > x[0]:
                        x[0] = ct + x[2]    # add interval
                        x[1]()              # call desired function

            time.sleep(0.1)


class EvolverFrame(wx.Frame):
    """The application window.
    This still uses CamelCase because that's what wx API uses..."""

    def __init__(self, canvas_size):
        wx.Frame.__init__(self, None, -1, "Evolver")

        # size for all images: smaller is, naturally, faster
        self.canvas_size = canvas_size

        self.profiler_stat_file = r'evolverstats'
        self.evolver = None

        self.running = False    # program state boolean

        self.CreateLayout()

        w, h = self.canvas_size
        self.ImageBest.SetBitmap(wx.Bitmap(w, h))

        self.updater = ImageUpdater()

        self.updater.schedule(self.UpdateBestImage, 1.0)
        self.updater.schedule(self.UpdateCurrentImage, 0.1)
        self.updater.start()

        self.SetSize((w * 3 + 40, h + 120))
        self.Bind(wx.EVT_CLOSE, self.OnClose)

    def OnClose(self, event):
        self.evolver.evolver_running = False
        self.updater.updater_running = False
        self.Destroy()

    def SetEvolver(self, evolver):
        """Set the evolver object."""
        # assign the ref image data and the shapes to use
        if self.evolver is not None:
            self.evolver.stop()

        self.evolver = evolver

        # also get current reference image from evolver
        self.ImageRef.SetBitmap(wx.Bitmap(evolver.refimage))

        self.evolver.start()

    def DestroyEvolver(self, event):
        self.evolver.terminate()

    def CreateLayout(self):
        MBox = wx.BoxSizer(wx.VERTICAL)
        self.SetSizer(MBox)

        # create the picture panel
        PPanel = wx.Panel(self)
        PPanel.SetBackgroundColour("#000000")
        MBox.Add(PPanel, proportion=0, flag=wx.EXPAND)
        PBox = wx.BoxSizer(wx.HORIZONTAL)
        PPanel.SetSizer(PBox)

        # create the image components with empty placeholder bitmaps
        self.ImageRef = wx.StaticBitmap(PPanel)
        PBox.Add(self.ImageRef, proportion=0, border=4, flag=wx.ALL | wx.EXPAND)
        self.ImageRef.SetBitmap(wx.Bitmap.FromRGBA(*self.canvas_size))

        self.ImageBest = wx.StaticBitmap(PPanel)
        PBox.Add(self.ImageBest, proportion=0, border=4, flag=wx.ALL | wx.EXPAND)
        self.ImageBest.SetBitmap(wx.Bitmap.FromRGBA(*self.canvas_size))

        self.ImageCurrent = wx.StaticBitmap(PPanel)
        PBox.Add(self.ImageCurrent, border=4, flag=wx.ALL | wx.EXPAND)
        self.ImageCurrent.SetBitmap(wx.Bitmap.FromRGBA(*self.canvas_size))

        # create the bottom panels
        TBox = wx.BoxSizer(wx.HORIZONTAL)
        BPanel = wx.Panel(self)
        BPanel.SetBackgroundColour("#BBBBBB")
        SPanel = wx.Panel(self)
        SPanel.SetBackgroundColour("#BBBBBB")
        MBox.Add(TBox, proportion=1, flag=wx.EXPAND)
        TBox.Add(BPanel, proportion=2, flag=wx.EXPAND)
        TBox.Add(SPanel, proportion=3, flag=wx.EXPAND)

        # buttons to button panel
        BPanel.SetSizer(wx.GridSizer(2, 2, 2, 2))
        ButtonIterate = wx.Button(BPanel, label="Iterate")
        BPanel.GetSizer().Add(ButtonIterate)
        ButtonIterate.Bind(wx.EVT_BUTTON, self.Iterate)

        self.ButtonRun = wx.Button(BPanel, label="RUN")
        BPanel.GetSizer().Add(self.ButtonRun)
        self.ButtonRun.Bind(wx.EVT_BUTTON, self.ToggleRun)

    def ToggleRun(self, event):
        """Start or stop running."""
        self.evolver.paused = not self.evolver.paused
        self.updater.paused = self.evolver.paused

        self.ButtonRun.SetLabel(["STOP", "RUN"][self.evolver.paused])
        self.Iterate(0)

        if self.evolver.paused:
            self.UpdateBestImage()
            self.UpdateCurrentImage()

            if PROFILING:
                self.evolver.dump_profiler_data(self.profiler_stat_file)
                s = pstats.Stats(self.profiler_stat_file)
                s.strip_dirs().sort_stats('tottime').print_stats(15)
        else:
            self.evolver.time_started = time.time()

    def UpdateBestImage(self):
        """Updates the middle image with the best scene found by evolver."""
        if not self.evolver:
            return

        image = self.evolver.bestimage
        if image:
            self.ImageBest.SetBitmap(self.evolver.painter.image2wxbitmap(image))
            self.Refresh()

    def UpdateCurrentImage(self):
        """Updates the rightmost image with the current scene in evolver."""
        if not self.evolver:
            return

        image = self.evolver.currentimage
        if image:
            self.ImageCurrent.SetBitmap(self.evolver.painter.image2wxbitmap(image))
            self.Refresh()

    def Iterate(self, event):
        """Do a single iteration in evolver and update resulting images."""
        self.evolver.iterate(0)
        self.UpdateBestImage()
        self.UpdateCurrentImage()
        self.Refresh()


class EvolverApp(wx.App):
    def OnInit(self, *args):
        """Initialize the application."""
        # TODO insert command-line arguments
        self.locale = wx.Locale(wx.LANGUAGE_ENGLISH)
        return True

    def SetEvolver(self, evolver):
        if self.frame is not None:
            self.frame.SetEvolver(evolver)
            self.frame.Iterate(0)


if __name__ == "__main__":
    """Main program. Adjust shape counts or canvas size, change the reference
    image or plug in your own evolver."""
    canvas_size = (200, 200)

    painter = None

    # create a painter object capable of painting the scene
    # PillowPainter is dreadfully slow, so use CairoPainter if you only can!
    try:
        import CairoPainter
        painter = CairoPainter.CairoPainter(canvas_size)
    except Exception:
        print "Failed to construct pycairo painter"

    if painter is None:
        try:
            import PillowPainter
            painter = PillowPainter.PillowPainter(canvas_size)
        except Exception:
            print "Failed to construct Pillow painter"

    if painter is None:
        print "No painter was constructed. Please install pycairo!"
        sys.exit(0)

    app = EvolverApp(0)

    image_file = 'blake.png'
    if len(sys.argv) > 1:
        image_file = sys.argv[1]

    reference_image = wx.Image(image_file).Rescale(*canvas_size)

    frame = EvolverFrame(canvas_size)
    app.frame = frame
    app.frame.Show(True)

    # create shapes
    shapedict = {
        primitives.Polygon: 50,
        primitives.Circle: 0,
        primitives.Line: 0,
        primitives.Rectangle: 0,
        primitives.Square: 0,
    }

    # create evolver object
    evolver = Evolver(reference_image, canvas_size, painter, shapedict)

    # an alternate evolver
    # from evolver import CrosshatchEvolver
    # evolver = CrosshatchEvolver(reference_image, canvas_size, painter)

    app.SetEvolver(evolver)

    app.MainLoop()
