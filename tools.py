"""
General tool methods.
"""


def f2i(rgb):
    """Convert a color from floats [0.0-1.0] to ints [0-255]."""
    return tuple(map(lambda x: int(x * 255), rgb))


def i2f(rgb):
    """Convert a color from ints [0-255] to floats [0.0-1.0]."""
    return tuple(map(lambda x: x / 255.0, rgb))


def clamp(x, xmin, xmax):
    """Clamp a value between minimum and maximum."""
    return min(max(x, xmin), xmax)


class switch(object):
    def __init__(self, value):
        self.value = value
        self.fallthrough = False

    def __iter__(self):
        yield self.matching
        raise StopIteration

    def matching(self, *args):
        if (not args) or self.fallthrough:
            return True
        elif self.value in args:
            self.fallthrough = True
            return True
        else:
            return False


if __name__ == '__main__':
    for c in 'abq?x':
        for case in switch(c):
            if case('a'):
                print 'a'
                break
            if case('q'):
                print 'q'
                break
            if case('x'):
                print 'x'
                break
            if case():
                print 'unknown'
                break
