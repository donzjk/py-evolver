# DEVELOPMENT IDEAS:
# - uniform (greyscale) color mutation
# - ellipses as primitives
# - Haskell translation!?
# - auto image save when paused
# - UI development
# - put Primitives in a separate file
# - instead of possibly mutating one shape per tick, mutate them all with a lower probability
#    -> painting and difference computation are the bottlenecks anyway, so it's more economical to
#       mutate multiple shapes per tick
# - different degrees of mutation (from a slight nudge/change in hue to a complete re-randomization)
#    -> hardness of mutation could be a float value instead of soft/hard
# - changing Z ordering in several ways (topmost-to-bottom, reshuffle, reverse order)
# - mutate from one primitive to another?!
# - or spontaneously create/destroy primitives?
# - allow coordinates to go off-screen (completely off-screen primitives should be dealt with tho)
# - parallelize for multithreading processors
#    -> difference computation can be easily parallelized: split the bitmap into N parts
#    -> mutation as well; split the object list into N parts
# - auto-stop when target reached (goal value for difference, N rounds of iteration etc.)

from random import random, randint, choice, shuffle
import time
import threading
import cProfile
import struct
import numpy

# hardness of mutation
M_HARD, M_SOFT = range(2)


# A few difference functions

def linear_diff(refdata, imgdata):
    """Compute linear difference between two images: sum of pixelwise and
    componentwise differences in RGB values."""
    return numpy.abs(refdata - imgdata).sum()


def quadratic_diff(refdata, imgdata):
    """Compute quadratic difference between two images."""
    return ((refdata - imgdata) ** 2).sum()


def valeur_diff(refdata, imgdata):
    """Compute 'valeur' difference (difference in RGB sums), not componentwise"""
    return numpy.abs(refdata.sum(axis=1) - imgdata.sum(axis=1)).sum()


class Evolver(threading.Thread):
    """Evolver deals with mutating the primitive shapes and comparing the
    resulting scene with the reference image."""
    def __init__(self, refimage, canvas_size, painter, shapedict):
        threading.Thread.__init__(self)

        self.shapes = []
        for shape, count in shapedict.items():
            self.shapes += [shape.create_random(canvas_size)
                            for _ in range(count)]

        shuffle(self.shapes)

        self.difference = None          # difference between current and reference
        self.canvas_size = canvas_size  # physical canvas_size of the image

        self.refimage = refimage

        # store the raw RGB data from the reference image
        refdata = refimage.GetData()
        refdata = struct.unpack('>%dB' % len(refdata), refdata)
        self.refdata = numpy.array(refdata).reshape((len(refdata) / 3, 3))

        # maximal difference value to compute percentage with
        # TODO this only gives correct values with linear_diff
        self.max_diff = sum(map(len, self.refdata)) * 256

        self.loops = 0              # number of iterations
        self.time_started = 0       # starting timestamp

        # define painter object to render the scene
        self.painter = painter

        self.bestimage = None       # the best image we've hit
        self.currentimage = None    # updated on each iteration

        # select difference function to use
        self.diff_function = linear_diff

        self.paused = True          # activity flag for thread

    def iterate(self, m_degree):
        """Do a single iteration: mutate the shapes and compare the resulting
        image with the reference (and the previous iteration). Keep if better,
        rollback if not."""
        self.loops += 1

        # evolve the scene
        z_mutate = random() < 0.05  # if true, mutate Z order instead of shapes

        if z_mutate:
            # mutate the Z order
            shapes_copy = self.shapes[:]
            i = randint(0, len(self.shapes) - 1)
            shape = self.shapes.pop(i)
            i = randint(0, len(self.shapes) - 1)
            self.shapes.insert(i, shape)
        else:
            # mutate a random shape
            shape = choice(self.shapes)
            shape.mutate(m_degree)

        # redraw current scene
        self.currentimage = self.painter.paint_scene(self.shapes)

        # compute current difference
        current_fitness = self.compute_fitness(self.currentimage)

        if self.difference is None or current_fitness <= self.difference:
            # beneficial mutation or first iteration
            self.difference = current_fitness
            self.bestimage = self.currentimage
            closeness = 100 * ((self.max_diff - self.difference) / float(self.max_diff))
            params = (self.loops, self.difference, closeness, self.get_frame_rate())
            print "#%d difference %d   closeness %.2f%%   frame rate %.2f" % params
        else:
            # reverse a detrimental mutation
            if z_mutate:
                self.shapes = shapes_copy
            else:
                shape.restore_state()

    def compute_fitness(self, image):
        """Compute difference for the current scene."""
        if self.refdata is None:
            raise Exception("No reference image set")

        imgdata = self.painter.get_rgb_data()
        return self.diff_function(self.refdata, imgdata)

    def run(self):
        self.profiler = cProfile.Profile()
        self.profiler.enable()

        self.evolver_running = True

        while self.evolver_running:
            if not self.paused:
                self.iterate(M_SOFT if random() < 0.9 else M_HARD)
                time.sleep(0.001)
            else:
                time.sleep(0.1)

        print "goodbye"

    def get_frame_rate(self):
        """Compute current iteration "frame rate". (We don't actually draw the
        scene in every frame)."""
        if not self.time_started:
            return 0.00
        return self.loops / (time.time() - self.time_started)

    def dump_profiler_data(self, fname):
        if self.profiler:
            self.profiler.disable()
            self.profiler.dump_stats(fname)

    def terminate(self):
        self.evolver_running = False


class CrosshatchEvolver(Evolver):
    """Evolver with a grid of crosshatches instead of mutable geometrical
    shapes."""
    def __init__(self, refimage, canvas_size, painter):
        from primitives import CrosshatchLine
        Evolver.__init__(self, refimage, canvas_size, painter, shapedict={})
        w, h = canvas_size
        width = 3

        for x in range(0, w, width):
            o = CrosshatchLine(canvas_size, (x, 0), (x, h), width=width)
            self.shapes.append(o)

        for y in range(0, h, width):
            o = CrosshatchLine(canvas_size, (0, y), (w, y), width=width)
            self.shapes.append(o)

        shuffle(self.shapes)
