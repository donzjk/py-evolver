"""A simple override decorator to better keep track of what's been overridden
from parent class."""


def overrides(parent):

    def override(method):
        assert method.__name__ in dir(parent), \
            "method '%s' not defined in superclass" % method.__name__
        return method

    return override
