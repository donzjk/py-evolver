A program that attempts to replicate a given picture with primitive geometric shapes using genetic algorithm with mutation and fitness functions.

Python 2.7, requires NumPy, wxPython and a graphics library (pycairo or Pillow).