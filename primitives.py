from abc import ABCMeta, abstractmethod
import math
import numpy
from random import random, choice, randint
from tools import clamp
from override import overrides

M_HARD, M_SOFT = (0, 1)


class Primitive(object):
    """All shape objects are inherited from the abstract Primitive."""
    __metaclass__ = ABCMeta

    def __init__(self, canvas_size, color=None, alpha=None):
        if color is None:
            self.color = numpy.array((random(), random(), random()))
        else:
            self.color = numpy.array(color)

        if alpha is None:
            self.alpha = random()
        else:
            self.alpha = alpha

        self.savedstate = None
        # the global canvas_size
        self.canvas_size = canvas_size

    @abstractmethod
    def create_random(self, size):
        """Create a random instance of this type within the given coordinates."""
        pass

    def mutate_color(self, m_degree=M_SOFT):
        """Mutate color of this shape. This is global, no need to override."""
        f = 0.4
        if random() < 0.25:
            # mutate alpha
            if m_degree == M_HARD:
                self.alpha = random()
            elif m_degree == M_SOFT:
                MIN_ALPHA = 0.0
                MAX_ALPHA = 1.0
                d = f * random() - (0.5 * f)
                if (self.alpha == MIN_ALPHA and d < 0) or (self.alpha == MAX_ALPHA and d > 0):
                    d = -d
                self.alpha = clamp(self.alpha + d, MIN_ALPHA, MAX_ALPHA)
        else:
            # mutate R, G and B
            for i in range(3):
                if m_degree == M_HARD:
                    self.color[i] = random()
                elif m_degree == M_SOFT:
                    d = f * random() - (0.5 * f)
                    if (self.color[i] == 0.0 and d < 0) or (self.color[i] == 1.0 and d > 0):
                        d = -d

                    self.color[i] = clamp(self.color[i] + d, 0.0, 1.0)

    @abstractmethod
    def mutate_shape(self, m_degree):
        """All shapes must define their own mutation, naturally."""
        pass

    @abstractmethod
    def save_state(self):
        """Save state before mutating so we can roll it back if necessary."""
        pass

    @abstractmethod
    def restore_state(self):
        """Restore a previously stored state."""
        pass

    def mutate(self, m_degree):
        """Mutate either the shape or the color of this shape."""
        self.save_state()
        if random() < 0.5:
            self.mutate_shape(m_degree)
        else:
            self.mutate_color(m_degree)

    def random_x(self):
        """Return a random float between 0 and this primitive's width."""
        return int(self.canvas_size[0] * random())

    def random_y(self):
        """Return a random float between 0 and this primitive's height."""
        return int(self.canvas_size[1] * random())

    def get_rgba(self):
        """Return a tuple of RGBA values. Values are floats within [0.0, 1.0]."""
        return tuple(self.color) + (self.alpha,)


class CrosshatchLine(Primitive):
    """Crosshatch lines are lines that go over the picture from one edge to the
    opposite. Their position does not change, but color, width and z-order do."""
    def __init__(self, size, p1, p2, width=1.0, color=None, alpha=None):
        Primitive.__init__(self, size, color, alpha)
        self.p1 = p1
        self.p2 = p2
        self.width = width

    @classmethod
    def create_random(self, size):
        w, h = size
        if random() < 0.5:
            y = random() * size[1]
            o = CrosshatchLine(size, (0, y), (w, y))
            return o
        else:
            x = random() * size[0]
            o = CrosshatchLine(size, (x, 0), (x, h))
            return o

    @overrides(Primitive)
    def mutate_shape(self, m_degree):
        if m_degree == M_SOFT:
            self.width += choice([1.0, -1.0])
        elif m_degree == M_HARD:
            self.width += choice(range(-5, 6))
        self.width = clamp(self.width, 1.0, 100.0)

    @overrides(Primitive)
    def save_state(self):
        self.savedstate = (self.p1, self.p2, self.width, self.color.copy(), self.alpha)

    @overrides(Primitive)
    def restore_state(self):
        if not self.savedstate:
            return
        self.p1, self.p2, self.width, self.color, self.alpha = self.savedstate
        self.savedstate = None

    def paint_cairo(self, dc):
        r, g, b = self.color

        dc.set_source_rgba(r, g, b, self.alpha)
        dc.set_line_width(self.width)

        (x0, y0) = self.p1
        (x1, y1) = self.p2
        dc.move_to(x0, y0)
        dc.line_to(x1, y1)
        dc.stroke()


class Line(Primitive):
    def __init__(self, canvas_size, p1, p2, color=None, alpha=None):
        Primitive.__init__(self, canvas_size, color, alpha)
        self.points = numpy.array([p1, p2])
        self.width = random() * min(self.canvas_size) * 0.3

    @classmethod
    def create_random(self, size):
        o = Line(size, (0, 0), (0, 0))
        p0 = o.random_point(size)
        p1 = o.random_point(size)
        o.points = numpy.array([p0, p1])

        return o

    def random_point(self, size):
        x = self.random_x()
        y = self.random_y()
        return (x, y)

    @overrides(Primitive)
    def save_state(self):
        self.savedstate = (self.points.copy(), self.width, self.color.copy(), self.alpha)

    @overrides(Primitive)
    def restore_state(self):
        if not self.savedstate:
            return
        self.points, self.width, self.color, self.alpha = self.savedstate
        self.savedstate = None

    @overrides(Primitive)
    def mutate_shape(self, m_degree):
        if random() < 0.3:
            # mutate line width
            if m_degree == M_HARD:
                self.width = random() * min(self.canvas_size) * 0.3
            elif m_degree == M_SOFT:
                self.width *= (random() * 0.4 + 0.8)
        else:
            # mutate a random endpoint
            i = randint(0, 1)
            self.points[i] = self.random_point(self.canvas_size)

    def __str__(self):
        return "(%d, %d)-(%d, %d) %d %s %f" % (tuple(self.points.flatten()) +
                                               (self.width, str(self.color), self.alpha))


class Polygon(Primitive):
    def __init__(self, canvas_size, vertices, color=None, alpha=None):
        Primitive.__init__(self, canvas_size, color, alpha)
        self.vertices = vertices

    @classmethod
    def create_random(self, size, num_vertices=3, color=None, alpha=None):
        if color is None:
            color = numpy.array([random(), random(), random()])
        if alpha is None:
            alpha = random()

        o = Polygon(size, None, color=color, alpha=alpha)
        o.vertices = numpy.array([(o.random_x(), o.random_y()) for _ in range(num_vertices)])

        return o

    @overrides(Primitive)
    def mutate_shape(self, m_degree):
        self.mutate_vertex(m_degree)

    def mutate_vertex(self, m_degree):
        # sometimes add or remove a vertex to/from a polygon
        # TODO maybe put this behind a flag?
        if random() < 0.2:
            # add or remove a vertex
            if len(self.vertices) == 3 or random() < 0.5:
                # add
                i = randint(0, len(self.vertices) - 1)
                self.vertices = numpy.insert(self.vertices, i,
                                             (self.random_x(), self.random_y()), 0)
            else:
                # remove
                i = randint(0, len(self.vertices) - 1)
                self.vertices = numpy.delete(self.vertices, i, 0)

        else:
            # move a vertex
            i = randint(0, len(self.vertices) - 1)
            if m_degree == M_HARD:
                # this is a relatively "hard" mutation as it completely randomizes the vertex
                # instead of nudging it
                self.vertices[i] = (self.random_x(), self.random_y())
            elif m_degree == M_SOFT:
                x, y = self.vertices[i]
                f = 0.4
                xd = int(f * self.random_x() - (0.5 * f * self.canvas_size[0]))
                yd = int(f * self.random_y() - (0.5 * f * self.canvas_size[1]))
                self.vertices[i] = (clamp(x + xd, 0, self.canvas_size[0]),
                                    clamp(y + yd, 0, self.canvas_size[1]))

    @overrides(Primitive)
    def save_state(self):
        self.savedstate = (self.vertices.copy(), self.color.copy(), self.alpha)

    @overrides(Primitive)
    def restore_state(self):
        if not self.savedstate:
            return
        self.vertices, self.color, self.alpha = self.savedstate
        self.savedstate = None

    def __str__(self):
        return "[%s, %s, %f]" % (str(self.vertices), str(self.color), self.alpha)


class Circle(Primitive):
    def __init__(self, size, center, radius, color=None, alpha=None):
        Primitive.__init__(self, size, color, alpha)
        self.center = center
        radius = clamp(radius, 5, min(size))
        self.radius = radius

    @classmethod
    @overrides(Primitive)
    def create_random(self, size, color=None, alpha=None):
        radius = int(random() * random() * 0.25 * min(size))
        o = Circle(size, None, radius, color, alpha)
        o.center = numpy.array([o.random_x(), o.random_y()])
        return o

    @overrides(Primitive)
    def mutate_shape(self, m_degree):
        if random() < 0.5:
            # mutate centerpoint
            if m_degree == M_HARD:
                self.center = numpy.array([self.random_x(), self.random_y()])
            elif m_degree == M_SOFT:
                self.center[0] += (0.2 * random() - 0.1) * self.canvas_size[0]
                self.center[0] = clamp(self.center[0], 0, self.canvas_size[0])
                self.center[1] += (0.2 * random() - 0.1) * self.canvas_size[1]
                self.center[1] = clamp(self.center[1], 0, self.canvas_size[1])
        else:
            # mutate radius
            if m_degree == M_HARD:
                self.radius = int(random() * random() * 0.25 * min(self.canvas_size))
            elif m_degree == M_SOFT:
                self.radius += (0.2 * random() - 0.1) * min(self.canvas_size)

            self.radius = clamp(self.radius, 5, min(self.canvas_size))

    @overrides(Primitive)
    def save_state(self):
        self.savedstate = (self.center.copy(), self.radius, self.color.copy(), self.alpha)

    @overrides(Primitive)
    def restore_state(self):
        if not self.savedstate:
            return
        self.center, self.radius, self.color, self.alpha = self.savedstate
        self.savedstate = None

    def get_scaled_center(self, w, h):
        x, y = self.center
        return (w * x, h * y)

    def get_scaled_radius(self, w, h):
        return max(w, h) * self.radius

    def __str__(self):
        x, y = self.center
        return "[(%d, %d), %d] " % (x, y, self.radius) + str(self.color)


class Square(Primitive):
    """Square with rotation"""
    def __init__(self, size, center, side, angle, color=None, alpha=None):
        Primitive.__init__(self, size, color, alpha)
        self.center = center
        self.side = side
        self.angle = angle
        self.vertices = []

    @classmethod
    @overrides(Primitive)
    def create_random(self, size, color=None, alpha=None):
        if color is None:
            color = numpy.array([random(), random(), random()])
        if alpha is None:
            alpha = random()

        side = int(random() * min(size))
        x = int(random() * size[0])
        y = int(random() * size[1])
        angle = random() * math.pi / 2
        o = Square(size, (x, y), side, angle, color, alpha)
        o.compute_vertices()
        return o

    def compute_vertices(self):
        vertices = []
        x, y = self.center
        hside = self.side / 2
        for a in [self.angle + n * math.pi / 2 for n in range(4)]:
            vertices.append((x + math.cos(a) * hside, y + math.sin(a) * hside))

        self.vertices = numpy.array(vertices)

    @overrides(Primitive)
    def mutate_shape(self, m_degree):
        r = random()

        # move
        if r < 0.3 or M_HARD:
            self.center = (self.random_x(), self.random_y())

        # change square_size
        if 0.3 < r < 0.7 or M_HARD:
            self.side = int(random() * min(self.canvas_size))

        # rotate
        if 0.7 < r or M_HARD:
            self.angle = random() * math.pi / 2

        # recompute vertices
        self.compute_vertices()

    @overrides(Primitive)
    def save_state(self):
        self.savedstate = (self.center, self.side, self.angle, self.color.copy(),
                           self.alpha, self.vertices)

    @overrides(Primitive)
    def restore_state(self):
        if not self.savedstate:
            return

        self.center, self.side, self.angle, self.color, self.alpha, self.vertices = self.savedstate
        self.savedstate = None


class Rectangle(Primitive):
    """Orthogonal rectangle, no rotation"""
    def __init__(self, size, p1, p2, color=None, alpha=None):
        Primitive.__init__(self, size, color, alpha)
        self.x1, self.y1 = p1
        self.x2, self.y2 = p2

    @classmethod
    def create_random(self, size, color=None, alpha=None):
        if color is None:
            color = numpy.array([random(), random(), random()])
        if alpha is None:
            alpha = random()

        o = Rectangle(size, (0, 0), (0, 0), color, alpha)
        o.x1, o.y1 = (o.random_x(), o.random_y())
        o.x2, o.y2 = (o.random_x(), o.random_y())

        return o

    @overrides(Primitive)
    def mutate_shape(self, m_degree):
        r = random()
        if r < 0.5 or M_HARD:
            self.x1 = self.random_x()
            self.y1 = self.random_y()
        if r >= 0.5 or M_HARD:
            self.x2 = self.random_x()
            self.y2 = self.random_y()

    @overrides(Primitive)
    def save_state(self):
        self.savedstate = (self.x1, self.y1, self.x2, self.y2, self.color.copy(), self.alpha)

    @overrides(Primitive)
    def restore_state(self):
        if not self.savedstate:
            return

        self.x1, self.y1, self.x2, self.y2, self.color, self.alpha = self.savedstate
        self.savedstate = None
